<?php
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Routing\Route;
use Illuminate\Session\SessionManager;
use Illuminate\Session\Store;
use Illuminate\Validation\Factory;
use Illuminate\Validation\Validator;

class Admin_UsersController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		if (isset($_GET["name"])) {
			$buscar = htmlspecialchars(Input::get("name"));
			$users = User::where('nombre', 'LIKE', '%'.$buscar.'%')->orwhere('email', 'LIKE', '%'.$buscar.'%')->paginate(7);
			return View::make('admin/users/list')->with('users', $users);

		}
		else {
			$users = User::paginate(7);
			return View::make('admin/users/list')->with('users', $users);
		}

	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		// Creamos un nuevo objeto User para ser usado por el helper Form::model
		$user = new User;
		return View::make('admin/users/form')->with('user', $user);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

	  $user = new User;
	  $data = Input::all();
		// Validate data
	  if ($user->isValid($data)==true)
	  {
	      $user->fill($data);
	      $user->save();
	      return Redirect::route('admin.users.show', array($user->id));
	  }
	  else
	  {
				return Redirect::route('admin.users.create')->withInput()->withErrors($user->errors);
	  }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user = User::find($id);

        if (is_null($user)) App::abort(404);

    return View::make('admin/users/show', array('user' => $user));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = User::find($id);
    if (is_null ($user))
    {
        App::abort(404);
    }

    $form_data = array('route' => array('admin.users.update', $user->id), 'method' => 'PATCH');
    $action    = 'Editar';

    return View::make('admin/users/form', compact('user', 'form_data', 'action'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
    $user = User::find($id);

    if (is_null ($user))
    {
        App::abort(404);
    }

    $data = Input::all();

    if ($user->isValid($data))
    {
        $user->fill($data);
        $user->save();
        return Redirect::route('admin.users.show', array($user->id));
    }
    else
    {
        return Redirect::route('admin.users.edit', $user->id)->withInput()->withErrors($user->errors);
    }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		$user = User::find($id);

    if (is_null ($user))
    {
        App::abort(404);
    }

		$user->delete();
    return Redirect::route('admin.users.index');
	}


}
