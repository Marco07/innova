<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
 public function up()
 {
     /**
      * Nota: no se ha traducido el nombre de la tabla, ya que Laravel incluye por defecto un modelo para
      * que esta guía de inicio rápido funcione como corresponde.
      */
     Schema::create('users', function($table)
     {
         $table->increments('id');
         $table->string('email')->unique();
         $table->string('nombre');
         $table->timestamps();
     });
 }

 public function down()
 {
     Schema::drop('users');
 }

}
