<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
	protected $perPage = 5;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
	protected $fillable = array('email', 'nombre', 'password');


	public $errors;

  public function isValid($data)
  {
	$rules = array(
            'email'     => 'required|email|unique:users',
            'nombre' => 'required|min:4|max:40',
            'password'  => 'min:8|confirmed'
        );

  if ($this->exists)
  {
			$rules['email'] .= ',email,' . $this->id;
  }
  else
  {
      $rules['password'] .= '|required';
  }

  $validator = Validator::make($data, $rules);

  if ($validator->passes())
  {
      return true;
  }

  $this->errors = $validator->errors();

  return false;

}

public function validAndSave($data)
{
	 if ($this->isValid($data))
	 {
			 $this->fill($data);
			 $this->save();

			 return true;
	 }

	 return false;
}

public function setPasswordAttribute($value)
{
		if ( ! empty ($value))
		{
				$this->attributes['password'] = Hash::make($value);
		}
}
}
