<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::resource('admin/users', 'Admin_UsersController');
Route::get('/', function()
{

	$users = User::paginate();
	return View::make('admin/users/list')->with('users', $users);
});
