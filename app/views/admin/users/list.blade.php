@extends ('admin/layout')

@section ('title') Lista de Usuarios @stop

@section ('content')

<div class="" id="wraper">
  <!-- Header (Title) -->
  <div align="center">
    <h1>Lista de usuarios</h1>
  </div>
  <!-- end header -->

  <!-- Body -->
  <div>
    <!-- Search bar -->
    <div>
      <hr style="margin: 5px;">
        <div class="row">
          {{ Form::open(['route' => 'admin.users.index', 'method' => 'GET', 'class' => 'navbar-form navbar-left pull-right', 'role' => 'search'])}}
          {{Form::text('name', null,  array('class' => 'form-control', 'placeholder' => 'Buscar'))}}
          {{Form::input('submit', null, 'Buscar', array('class' => 'btn btn-primary'))}}
          {{Form::close()}}

        </div>
      <hr style="margin-top: 5px;">
    </div>
    <!-- End search bar -->

    <!-- User's table -->
    <div>
     <table class="table table-striped">
        <tr>
          <th>Nombre</th>
          <th>Email</th>
          <th></th>
          <th></th>
          <th></th>
        </tr>
      @foreach ($users as $user)
        <tr>
          <td>{{ $user->nombre }}</td>
          <td>{{ $user->email }}</td>
          <td>
            {{ Form::model($user, array('route' => array('admin.users.destroy', $user->id), 'method' => 'DELETE', 'role' => 'form')) }}
            <div class="row">
              <div class="form-group col-md-4">
                  {{ Form::submit('Eliminar', array('class' => 'btn btn-danger')) }}
              </div>
            </div>
            {{ Form::close() }}
          </td>
          <td>
            <a href="{{ route('admin.users.edit', $user->id) }}" class="btn btn-primary">
              Editar
            </a>
          </td>
          <td>
            <a href="{{ route('admin.users.show', $user->id) }}" class="btn btn-info">
                Ver
            </a>
          </td>
        </tr>
      @endforeach
     </table>
    </div>
    <!-- End user's table -->

    <!-- Pagination and create users -->
    <div style="width: 100%;
      height: 100px;
      position: absolute;
      bottom: 35px;
      left: 0;
      padding: 20px;" class="row" align="center">
      <div class="col-sm-3">
        {{ $users->links() }}
      </div>
      <div align="right" style="margin-top: 20px;">
        <p>
          <a href="{{ route('admin.users.create') }}" class="btn btn-primary">Crear un nuevo usuario</a>
        </p>
      </div>
    </div>
  </div>
</div>

@stop
